import unittest

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By


class TestIndexhtml(unittest.TestCase):

    def setUp(self):
        options = webdriver.ChromeOptions()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        service = Service(executable_path=r'/usr/bin/chromedriver')
        self.driver = webdriver.Chrome(service=service, options=options)

    def test_page(self):
        self.driver.get(f"file:///usr/lib/cyberchef/index.html")

        results = WebDriverWait(self.driver, 10).until(lambda x:
                                                       x.find_element(By.ID,
                                                                      "recipe"))
        self.assertIn("Recipe", results.text)
        self.assertIn("BAKE", results.text)

        sidebar = WebDriverWait(self.driver, 10).until(lambda x:
                                                       x.find_element(By.ID,
                                                                      "operations"))
        self.assertIn("To Hex", sidebar.text)
        self.assertIn("Encryption / Encoding", sidebar.text)


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestIndexhtml)
    unittest.TextTestRunner(verbosity=2).run(suite)
